﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMove : MonoBehaviour
{
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        //        this.transform.Translate(new Vector3(Random.Range(-10.0f, 10.0f), 0, Random.Range(-10.0f, 10.0f)));
        //              this.transform.Rotate(Vector3.up, Random.Range(-180, +180));

                this.transform.Translate(new Vector3(1f,0f,0f));
        rb = gameObject.GetComponent<Rigidbody>();
            rb.velocity = new Vector3(Random.Range(5,15), 0,Random.Range(5,15));
    }

    // Update is called once per frame
    void Update()
    {
             rb.velocity = rb.velocity.normalized*7;
    }

    public void stop()
    {
         rb.constraints = RigidbodyConstraints.FreezePosition;
    }
}
