﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour
{
    public bool home = false;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void frena(double x, double z)
    {
        StartCoroutine(_frena(x, z));
    }

    private IEnumerator _frena(double x, double z)
    {
        bool inside = true;
        bool stopped = false;
        Rigidbody rb = this.GetComponent<Rigidbody>();
        Debug.Log("prewhile");

        while (inside && !stopped)
        {
            yield return new WaitForSeconds(0.1f);
            Debug.Log("inwhile");

            if (x > 0)
            {
                inside &= transform.position.x > x;
            }
            else
            {
                inside &= transform.position.x < x;
            }
            if (z > 0)
            {
                inside &= transform.position.z > z;
            }
            else
            {
                inside &= transform.position.z < z;
            }
            if (rb.velocity.magnitude < 0.1)
            {
                Debug.Log("Congelem: "+this.tag);
                rb.velocity = new Vector3(0, 0, 0);
                rb.constraints = RigidbodyConstraints.FreezePosition;
                stopped = true;
                home = true;
            }
            else
            {
                //                rb.velocity = rb.velocity / 2;
                rb.velocity =
                    rb.velocity.normalized *
                    Mathf.Lerp(rb.velocity.magnitude, 0.0f, 0.1f);
            }
            Debug
                .Log(this.tag+" -> x: " +
                transform.position.x +
                " z: " +
                transform.position.z+" velocitat: "+rb.velocity.magnitude);
            this.transform.Translate(new Vector3(0, 0, 0));
        }
    }
}
