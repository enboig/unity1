﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrogaCasa : MonoBehaviour
{
    private Rigidbody rb;

    public bool casa = false;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        // x < -9.5
        // z > 9.5
        if ((transform.position.x < -9.5) && (transform.position.z > 9.5))
        {
            if (rb.velocity.magnitude < 0.1)
            {
                rb.velocity = new Vector3(0, 0, 0);
                rb.constraints = RigidbodyConstraints.FreezePosition;
                casa = true;
            }
            else
            {
                rb.velocity = rb.velocity / 2;
            }

            Debug
                .Log("GrogaCasa -> x: " +
                transform.position.x +
                " z: " +
                transform.position.z +
                " velocity: " +
                rb.velocity.magnitude);
            //this.transform.Translate(new Vector3(0,0,0));
        }
    }
}
